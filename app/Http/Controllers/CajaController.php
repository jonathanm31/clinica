<?php 
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Productos;
use App\Models\Clientes;
use App\Models\Documento;
use App\Models\NotaCredito;
use App\Models\CajaD;
use App\Models\CajaH;
use App\User;
use Auth;
use Carbon\Carbon ;
use Illuminate\Support\Facades\Redirect;
use DB;



class CajaController extends Controller
{
     public function __construct(){
        $this->middleware('auth');
    }



    public function getProductos($categoria,$nombre){
       $productos = Productos::where('nombre','LIKE', '%'.$nombre.'%')->where('tipo', $categoria)->get();
        return json_encode($productos);
    }

    public function getAllProductos($nombre){
       $productos = Productos::where('nombre','LIKE', '%'.$nombre.'%')->get();
        return json_encode($productos);
    }

    public function getProductoid($id){
        $producto = Productos::find($id);
        return json_encode($producto);
    }

    public function getnumserie($tipo){
        $doc_anterior = Documento::where('tipo', $tipo)->orderBy('iddocumento', 'desc')->limit(1)->get();
        $num;
        foreach ($doc_anterior as $doc)
        {
           $num = $doc->numeracion;
           
        }
        $int_num = (int)$num;
        $int_num++;   

        $num = str_pad($int_num, 6, "0", STR_PAD_LEFT); 



        return $num;
    }

    public function guardarCajaFactura(Request $request){

        $idcliente = 0;
        $tipo = 1;   // 1 factura, 2 boleta, 3 nota de credito
        if (!$request['idcliente']){

            $cliente = new Clientes;
            $cliente->dni = $request['dni'];
            $cliente->nombre = $request['nombres_apell'];
            $cliente->direccion = $request['direccion'];
            $cliente->ruc = $request['ruc'];

            $cliente->save();
            $idcliente = $cliente->idcliente;
        }else{
            $idcliente = $request['idcliente'];
            $cli = Clientes::find($idcliente);
            if($cli->ruc == null){
                $cli->ruc = $request['ruc'];
                $cli->save();
            }

        }




        $cantidad = 0;

        foreach ($request['cantidades'] as $key => $value) {
            $cantidad += $value;
        }

        /***CREAR DOCUMENTO*****/

        $documento = new Documento;
        $documento->tipo = $tipo;   
        $documento->numeracion = $this->getnumserie(1);
        $user = $request->user();
        $documento->serie = $user->sede;
        $documento->save();
        

        /*********************/



        $caja = new CajaH;
        $caja->clienteH = $idcliente;
        $caja->cantidad = $cantidad;
        $caja->total = $request['total_detalle_t_i'];
        $caja->documento = $documento->iddocumento;
        $caja->state = 1;
        $caja->save();

        for($i = 0; $i < count($request['ids']); $i++){
            $cajaDetalle = new CajaD;
            $cajaDetalle->idcajaH = $caja->idcaja;
            $cajaDetalle->productosD = $request['ids'][$i];
            $cajaDetalle->cantidad = $request['cantidades'][$i];
            $cajaDetalle->precio = $request['precios'][$i];
            $cajaDetalle->save();            
        }
        $data = $caja->idcaja;

        return $data;
    }

    public function getInfoFactura($idFactura){

        $factura = CajaH::find($idFactura);
        $documento = Documento::find($factura->documento);
        $cliente = Clientes::find($factura->clienteH);
        $detalle = DB::table('cajaD')
                    ->select('cajaD.cantidad', 'cajaD.precio', 'productos.nombre')
                    ->where('idcajaH', $idFactura)
                    ->join('productos', 'cajaD.productosD', '=', 'productos.idproducto')->get();
   

        $datos =  array($factura, $documento, $cliente, $detalle);      
 
        return view('caja/detalleFactura')
                ->with('datos', $datos);
    }



    public function getAllDoc(){
    
      /*  $string_fecha = file_get_contents('http://time.bitsknow.com/time.php');

        $fecha = nl2br($string_fecha);
        $fecha = trim($fecha,  "<br /> \n.");
        $fecha = str_replace(PHP_EOL, '', $fecha);

        //var_dump($fecha1);*/
        $sede = Auth::user()->sede;


        /*echo $tomor.'<br>';
        echo $yest;*/
       $documentos = CajaH::whereRaw('Date(cajaH.created_at ) = CURDATE()')
       //where(DB::raw('DATE( cajaH.created_at ) = DATE(CURDATE()) ') )
                        ->join('clientes', 'cajaH.clienteH', '=', 'clientes.idcliente')
                        ->join('documentos', 'cajaH.documento', '=', 'documentos.iddocumento')                           
                        ->where('documentos.serie', '=', $sede )                  
                        ->paginate(15);
       

        return view('caja/listaDocumentos')
                ->with('documentos', $documentos);

    }


    public function guardarCajaBoleta(Request $request){

        $idcliente = 0;
        $tipo = 2;   // 1 factura, 2 boleta, 3 nota de credito

        if(!$request['dni'])return 0;
        if (!$request['idcliente']){

            $cliente = new Clientes;

            $cliente->dni = $request['dni'];

            $cliente->nombre = $request['nombres_apell'];
            $cliente->direccion = $request['direccion'];
           

            $cliente->save();
            $idcliente = $cliente->idcliente;
        }else{
            $idcliente = $request['idcliente'];          

        }

        $cantidad = 0;

        foreach ($request['cantidades'] as $key => $value) {
            $cantidad += $value;
        }

        /***CREAR DOCUMENTO*****/

        $documento = new Documento;
        $documento->tipo = $tipo;   
        $documento->numeracion = $this->getnumserie(2);
        $user = $request->user();
        $documento->serie = $user->sede;
        $documento->save();
        

        /*********************/



        $caja = new CajaH;
        $caja->clienteH = $idcliente;
        $caja->cantidad = $cantidad;
        $caja->total = $request['total_detalle_t_i'];
        $caja->documento = $documento->iddocumento;
        $caja->pago = $request['pago'];
        $caja->state = 1;
        $caja->save();

        for($i = 0; $i < count($request['ids']); $i++){
            $cajaDetalle = new CajaD;
            $cajaDetalle->idcajaH = $caja->idcaja;
            $cajaDetalle->productosD = $request['ids'][$i];
            $cajaDetalle->cantidad = $request['cantidades'][$i];
            $cajaDetalle->precio = $request['precios'][$i];
            $cajaDetalle->save();            
        }
        $data = $caja->idcaja;

        return $data;
    }
    public function getInfoBoleta($idBol){

        $boleta= CajaH::find($idBol);
        $documento = Documento::find($boleta->documento);
        $cliente = Clientes::find($boleta->clienteH);
        $detalle = DB::table('cajaD')
                    ->select('cajaD.cantidad', 'cajaD.precio', 'productos.nombre')
                    ->where('idcajaH', $idBol)
                    ->join('productos', 'cajaD.productosD', '=', 'productos.idproducto')->get();

        $datos =  array($boleta, $documento, $cliente, $detalle);      
 
        return view('caja/detalleBoleta')
                ->with('datos', $datos);
    }

    public function guardarCajaNota(Request $request){

        $idcliente = 0;
        $tipo = 3;   // 1 factura, 2 boleta, 3 nota de credito
        if (!$request['idcliente']){

            $cliente = new Clientes;
            $cliente->dni = $request['dni'];
            $cliente->nombre = $request['nombres_apell'];
            $cliente->direccion = $request['direccion'];
            $cliente->ruc = $request['ruc'];

            $cliente->save();
            $idcliente = $cliente->idcliente;
        }else{
            $idcliente = $request['idcliente'];
            $cli = Clientes::find($idcliente);
            if($cli->ruc == null){
                $cli->ruc = $request['ruc'];
                $cli->save();
            }

        }




        $cantidad = 0;

        foreach ($request['cantidades'] as $key => $value) {
            $cantidad += $value;
        }

        /********CREAR NOTA DE CREDITO*************/

        $nota = new NotaCredito;
        $nota->motivo = $request['motivo'];
        $nota->receptordni = $request['receptordni'];
        $nota->nombreapellido = $request['receptor'];
        $nota->denominacion = $request['denominacion'];
        $nota->num = $request['numero'];
        $nota->fecha_emision = $request['fecha_emision'];
        $nota->save();



        /***CREAR DOCUMENTO*****/

        $documento = new Documento;
        $documento->tipo = $tipo;   
        $documento->numeracion = $this->getnumserie(3);
        $documento->nota_credito = $nota->idCredito;
        $user = $request->user();
        $documento->serie = $user->sede;
        $documento->save();
        



        $caja = new CajaH;
        $caja->clienteH = $idcliente;
        $caja->cantidad = $cantidad;
        $caja->total = $request['total_detalle_t_i'];
        $caja->documento = $documento->iddocumento;
        $caja->state = 1;
        $caja->save();

        for($i = 0; $i < count($request['ids']); $i++){
            $cajaDetalle = new CajaD;
            $cajaDetalle->idcajaH = $caja->idcaja;
            $cajaDetalle->productosD = $request['ids'][$i];
            $cajaDetalle->cantidad = $request['cantidades'][$i];
            $cajaDetalle->precio = $request['precios'][$i];
            $cajaDetalle->save();            
        }
        $data = $caja->idcaja;

        return $data;
    }

    public function getInfoNota($idFactura){

        $factura = CajaH::find($idFactura);
        $documento = Documento::find($factura->documento);
        $cliente = Clientes::find($factura->clienteH);
        $detalle = DB::table('cajaD')
                    ->select('cajaD.cantidad', 'cajaD.precio', 'productos.nombre')
                    ->where('idcajaH', $idFactura)
                    ->join('productos', 'cajaD.productosD', '=', 'productos.idproducto')->get();
        $nota =  NotaCredito::find($documento->nota_credito);       

        $datos =  array($factura, $documento, $cliente, $detalle, $nota);      
 
        return view('caja/detalleNota')
                ->with('datos', $datos);

    }

    public function guardarBolMake(Request $request){
        
        $idcliente = 0;
        $tipo = $request['tipo'];   // 1 factura, 2 boleta, 3 nota de credito

        if (!$request['idcliente']){

            $cliente = new Clientes;
            $cliente->dni = $request['dni'];
            $cliente->nombre = $request['nombres_apell'];
            $cliente->direccion = $request['direccion'];
           

            $cliente->save();
            $idcliente = $cliente->idcliente;
        }else{
            $idcliente = $request['idcliente'];          

        }
  
        $cantidad = 0;

        foreach ($request['cantidades'] as $key => $value) {
            $cantidad += $value;
        }

        /***CREAR DOCUMENTO*****/

        $documento = new Documento;
        $documento->tipo = 4;   
        $documento->numeracion = $request['numer_bol'];
        //$documento->numeracion = "000245";
        $user = $request->user();
        //$documento->serie = 4;
        $documento->serie =  intval($request['serie_bol']);
        $documento->save();
        

        /*********************/



        $caja = new CajaH;
        $caja->clienteH = $idcliente;
        $caja->cantidad = $cantidad;
        $caja->total = $request['total_detalle_t_i'];
        $caja->documento = $documento->iddocumento;
        $caja->pago = $request['pago'];
        $caja->state = 1;
        $caja->save();

        for($i = 0; $i < count($request['ids']); $i++){
            $cajaDetalle = new CajaD;
            $cajaDetalle->idcajaH = $caja->idcaja;
            $cajaDetalle->productosD = $request['ids'][$i];
            $cajaDetalle->cantidad = $request['cantidades'][$i];
            $cajaDetalle->precio = $request['precios'][$i];
            $cajaDetalle->save();            
        }
        $data = $caja->idcaja;

        return $data;
    }



}