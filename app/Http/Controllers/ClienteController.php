<?php

namespace App\Http\Controllers;

use App\Models\Clientes;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class ClienteController extends Controller
{

	public function __construct(){
        $this->middleware('auth');
    }
    
    public function getInfoCliente($dni){
        $cliente = Clientes::where('dni','=',$dni)->get();
        if(count($cliente)>0){
            return json_encode($cliente);
        }else{
            return 0;
        }
    }

    public function updateCliente(Request $request){
            $cliente = Clientes::find($request['idcliente']);
            $cliente->dni = $request['dni'];
            $cliente->nombre = $request['nombres_apell'];
            $cliente->direccion = $request['direccion'];
            //if($request['ruc'])$cliente->ruc = $request['ruc'];
            $cliente->save();
    }



}
