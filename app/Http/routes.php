<?php


/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/


Route::group(['middleware' => ['web']], function () {
    Route::get('/', function () {
        return view('index');
    });

});

Route::group(['middleware' => 'web'], function () {

    Route::auth();
    Route::get('/home', 'HomeController@index');
    ///////////////////////////REST para productos modulo Caja//////////////////7
    Route::get('get_productos/{categoria}/{nombre}','CajaController@getProductos');
    Route::get('get_all_productos/{nombre}','CajaController@getAllProductos');
    Route::get('get_producto_id/{id}','CajaController@getProductoid');
    Route::get('get_info_cliente/{dni}','ClienteController@getInfoCliente');
    Route::get('get_info_num/{tipo}','CajaController@getnumserie');
    ///////////////////////////REST FACTURA////////////////////////////

    Route::get('get_info_factura/{idFactura}','CajaController@getInfoFactura');
    

    /////////////////////////REST BOLETA//////////////////////////////////

   
    Route::get('get_info_bol/{idFactura}','CajaController@getInfoBoleta');
    Route::get('get_all_doc/','CajaController@getAllDoc');

    //////////////////////REST NOTA DE CREDITO////////////////////////////

   
    Route::get('get_info_nota/{iddoc}','CajaController@getInfoNota');

       /**********GUARDAR******/
    Route::post('updateCliente/', array('uses' => 'ClienteController@updateCliente')); 
    Route::post('guardar_caja_fact', array('uses' => 'CajaController@guardarCajaFactura'));   
    //Route::post('guardar_caja_fact/','CajaController@guardarCajaFactura');
    Route::post('guardar_caja_bol/','CajaController@guardarCajaBoleta');
    /*Route::post('guardar_bol_make/','CajaController@guardarBolMake');*/
    Route::post('guardar_caja_nota/','CajaController@guardarCajaNota');
    

    Route::get('/caja', function () {
        return view('/../caja/formulario_pp');
    });
    Route::get('/caja1', function () {
        return view('/../caja/formulario');
    });

    /********facturas**********/

    Route::get('fact_prod/', function () {
        $data = (['tipo' => 1]);
        return view('/../caja/formulario_pp')->with('tipo', $data);;
    });
    Route::get('fact_servicio/', function () {
        $data = (['tipo' => 2]);
        return view('/../caja/formulario_pp')->with('tipo', $data);;
    });
    Route::get('fact_rayos/', function () {
        $data = (['tipo' => 3]);
        return view('/../caja/formulario_pp')->with('tipo', $data);;
    });

    /*******boletas***********/

    Route::get('bol_prod/', function () {
        $data = (['tipo' => 1]);
        return view('/../caja/formulario_boleta')->with('tipo', $data);;
    });
    Route::get('bol_servicio/', function () {
        $data = (['tipo' => 2]);
        return view('/../caja/formulario_boleta')->with('tipo', $data);;
    });
    Route::get('bol_rayos/', function () {
        $data = (['tipo' => 3]);
        return view('/../caja/formulario_boleta')->with('tipo', $data);;
    });
    /*Route::get('bol_make/', function () {
        $data = (['tipo' => 4]);
        return view('/../caja/formulario_make')->with('tipo', $data);;
    });*/

    /*******NOTAS DE CREDITO***********/

    Route::get('nota_prod/', function () {
        $data = (['tipo' => 1]);
        return view('/../caja/formulario_nota_credito')->with('tipo', $data);;
    });
    Route::get('nota_servicio/', function () {
        $data = (['tipo' => 2]);
        return view('/../caja/formulario_nota_credito')->with('tipo', $data);;
    });
    Route::get('nota_rayos/', function () {
        $data = (['tipo' => 3]);
        return view('/../caja/formulario_nota_credito')->with('tipo', $data);;
    });

    /******************************/
});
