<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CajaD extends Model
{
	protected $table = 'cajaD';
	protected $primaryKey = 'idcajaD';
    protected $fillable = ['idcajaH', 'productosD','cantidad', 'precio'];
    public $timestamps = false;

}