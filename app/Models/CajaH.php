<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CajaH extends Model
{
	protected $table = 'cajaH';
	protected $primaryKey = 'idcaja';
    protected $fillable = ['clienteH', 'cantidad', 'total', 'tipo','pago', 'state'];
    //pago : 1 visa, 2 mastercard, 3 american expres, 4 pago deposito, 5 efectivo
    public $timestamps = false;
}