<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Clientes extends Model
{
	protected $table = 'clientes';
	protected $primaryKey = 'idcliente';
    protected $fillable = ['dni', 'nombre', 'apellido', 'direccion', 'ruc'];
    public $timestamps = false;
}