<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Documento extends Model
{
    protected $table = 'documentos';
	protected $primaryKey = 'iddocumento';
    protected $fillable = ['tipo', 'numeracion', 'nota_credito', 'serie'];
    public $timestamps = false;
}
