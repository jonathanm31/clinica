<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;;

class NotaCredito extends Model
{
    protected $table = 'nota_creditos';
	protected $primaryKey = 'idCredito';
    protected $fillable = ['motivo', 'receptordni', 'nombreapellido', 'fecha', 'denominacion', 'num', 'fecha_emision'];
    public $timestamps = false;
}
