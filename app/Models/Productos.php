<?php 
namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Productos extends Model {
    protected $table = 'productos';
    protected $primaryKey = 'idproducto'; 
    protected $fillable = ['nombre','tipo',  'cantidad', 'precio', 'state'];
}