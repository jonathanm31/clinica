<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('productos', function (Blueprint $table) {
            $table->increments('idproducto');
            $table->string('nombre');
            $table->integer('tipo');// 1 producto, 2 plan de pago, 3 rayos x (categoria)
            $table->integer('cantidad');
            $table->float('precio');
            $table->boolean('state');
            $table->timestamp('created_at');
            $table->timestamp('update_at')->nullable();           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('productos');
    }
}
