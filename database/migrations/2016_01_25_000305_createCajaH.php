<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCajaH extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cajaH', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('idcaja');   
            $table->integer('clienteH')->unsigned()->index();
            $table->integer('cantidad');
            $table->integer('documento')->unsigned()->index();
            $table->float('total');            
            $table->boolean('state');
            $table->timestamp('created_at');
            $table->timestamp('update_at')->nullable();   
            $table->foreign('clienteH')->references('idcliente')->on('clientes'); 
            //$table->foreign('documento')->references('iddocumento')->on('documentos');   
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cajaH');
    }
}
 