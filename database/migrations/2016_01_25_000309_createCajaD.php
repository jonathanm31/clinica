<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCajaD extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cajaD', function (Blueprint $table) {
            $table->increments('idcajaD');   
            $table->integer('idcajaH')->unsigned();; 
            $table->integer('productosD')->unsigned();;
            $table->integer('cantidad');
            $table->float('precio');
            $table->timestamp('created_at');
            $table->timestamp('update_at')->nullable();   
            $table->foreign('idcajaH')->references('idcaja')->on('cajaH');   
            $table->foreign('productosD')->references('idproducto')->on('productos');   

        });    
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cajaD'); 
    }
}
