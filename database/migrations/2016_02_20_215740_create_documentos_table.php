<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDocumentosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('documentos', function (Blueprint $table) {
            $table->increments('iddocumento');
            $table->integer('tipo');// 1 factura, 2 boleta, 3 Nota de Credito
            $table->string('numeracion');            
            //$table->integer('nota_credito')->nullable()->unsigned()->index();          
            //$table->foreign('nota_credito')->references('idCredito')->on('nota_creditos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('documentos');
    }
}
