<!DOCTYPE html>
<html lang="en" class="no-js">
<head>
    <meta charset="utf-8"/>
    <title>Quiropractica</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport"/>
    <meta content="" name="description"/>
    <meta content="" name="author"/>
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
    <link href="{{ URL::asset('/themes/assets/global/plugins/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ URL::asset('/themes/assets/global/plugins/simple-line-icons/simple-line-icons.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ URL::asset('/themes/assets/global/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>
    
    <link href="{{ URL::asset('/themes/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ URL::asset('/themes/assets/global/css/components.css') }}" id="style_components" rel="stylesheet" type="text/css"/>
    <link href="{{ URL::asset('/themes/assets/global/css/plugins.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ URL::asset('/themes/assets/admin/layout/css/layout.css') }}" rel="stylesheet" type="text/css"/>
    
    <link href="{{ URL::asset('/themes/assets/admin/layout/css/custom.css') }}" rel="stylesheet" type="text/css"/>
    <link rel="shortcut icon" href="favicon.ico"/>
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('/themes/assets/global/css/print.css') }}" media="print" />
</head>

<body>
<div class="col-md-3 col-md-offset-6" >
    <button class ="btn green no-print"onclick="myFunction()"><i class="fa fa-print">  Imprimir  </i></button> 
</div>

<div class="col-md-12" >
<h1>Numero de Serie 00{{$datos[1]->serie}} - {{$datos[1]->numeracion}}</h1>
<b>Arequipa {{$datos[0]->created_at}}</b>
<h2>Datos del Cliente</h2>
<div class="table-responsive">
    <table class="table">
    <tbody>
        <tr>
        <td> <h4>DNI</h4> </td>
        <td>{{$datos[2]->dni}}</td>
        </tr>
        <tr>
        <td> <h4>RUC</h4> </td>
        <td> {{$datos[2]->ruc}}</td>
        </tr>
        <tr>
        <td><h4> Nombres y Apellidos </h4></td>
        <td> {{$datos[2]->nombre}}</td>
        </tr>
        <tr>
        <td><h4>Direccion </h4></td>
        <td> {{$datos[2]->direccion}}</td>
        </tr>
    </tbody>
</table>
</div>
</div>

<div class="col-md-12" >
<h2>Detalle de Factura</h2>
<div class="table-responsive">
<table class="table">
<tbody>
    <thead>
    <tr>
    <td><h4>Cantidad</h4></td>
    <td><h4>Detalle</h4></td>
    <td><h4>Precio</h4></td>
    <td><h4>Importe</h4></td>
    </tr>
    </thead>

     @foreach($datos[3]as $producto)
    <tr>
        <td>{{$producto->cantidad}}</td>
        <td>{{$producto->nombre}}</td>
        <td>{{$producto->precio}}</td>
        <td>
        <?php 
        $impor = ((int)$producto->cantidad * (float)$producto->precio) ; 
        echo number_format($impor, 2, '.', ' ');
        ?>
        </td>
    </tr>  
    @endforeach
</tbody>
</table>
</div>

<div class="col-md-12" >
<h2>Monto Total</h2>
<div class="table-responsive">
    <table class="table">
    <tbody>
        <tr>
        <td> <h4>Valor de Venta</h4> </td>
        <td><?PHP
        echo number_format((float)$datos[0]->total - ((float)$datos[0]->total * 0.18), 2, '.', ' ');
        ?>
        </td>
        </tr>
        <tr>
        <td> <h4>IGV</h4> </td>
        <td><?PHP
        echo number_format((float)$datos[0]->total * 0.18, 2, '.', ' ');
        ?>
        </td>
        </tr>
        <tr>
        <td><h4> Total </h4></td>
        <td> {{$datos[0]->total}}</td>
        </tr>
    </tbody>
</table>
</div>
</div>
<script>
function myFunction() {
    window.print();
}
</script>
</body>
</html>