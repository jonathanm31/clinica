    @extends('index')
    @section('contenido')
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="index.html">Home</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <span>Registro de Caja</span>
            </li>
        </ul>
    </div>
    <div class="col-md-12">
        <div class="portlet light bordered form-fit">
            <h3 class="page-title"> Emisión de Factura
                <small>
                    R.U.C. 20455371004</small>
            </h3>
            </h4>
            <h4 ><i>Arequipa 15 de Enero de 2016</i>
            </h4>
        </div>
    </div>
    <form id="form_caja" name="form_caja" method="POST" action="guardar_caja">
    <div class="col-md-8">
        <div class="portlet light bordered">
            <div class="portlet-body form">
                    <div class="form-body">
                    <h3 class="page-title"> Datos del Cliente </h3>
                        <div class="form-group">
                            <div class="input right margin-top-10 ">
                                <input type="text" max="99999999" class="form-control" id="dni" name="dni" placeholder="DNI">
                                <input type="hidden" id="idcliente" name="idcliente" value="0">
                            </div>

                            <div class="input right margin-top-10 ">
                                <input type="text" max="99999999" class="form-control" id="ruc" name="ruc" placeholder="RUC">
                            </div>

                            <div class="input-icon right margin-top-10">
                                <i class="fa fa-user"></i>
                                <input type="text" class="form-control" id="nombres_apell" name="nombres_apell" placeholder="Nombres y Apellidos">
                            </div>

                            <div class="input-icon right margin-top-10">
                                <i class="fa fa-home"></i>
                                <input type="text" class="form-control" id="direccion" name="direccion" placeholder="Direccion">
                            </div>
                        </div>
                    </div>
            </div>
        </div>
    </div>

    <div class="col-md-4">
        <div class="portlet light bordered">
            <div class="portlet box green">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-cogs"></i>DATOS FACTURA</div>
                                    <div class="tools">
                                        <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
                                        <a href="javascript:;" class="reload" data-original-title="" title=""> </a>
                                    </div>
                                </div>
                                <div class="portlet-body flip-scroll">
                                    <table class="table table-bordered table-striped table-condensed flip-content">

                                        <tbody>
                                            <tr>
                                                <td>VALOR DE VENTA </td>
                                                <td style="width: 50%;" id="total_detalle_c"></td>
                                                <input type="hidden" id="total_detalle_c_i" name="total_detalle_c_i" value="0">

                                            </tr>
                                            <tr>
                                                <td>IGV DEL 18%</td>
                                                <td style="width: 50%;" id="total_detalle_c_igv"></td>
                                                <input type="hidden" id="total_detalle_c_igv_i" name="total_detalle_c_igv_i" value="0">
                                            </tr>
                                            <tr>
                                                <td>TOTAL </td>
                                                <td style="width: 50%;" id="total_detalle_t"></td>
                                                <input type="hidden" id="total_detalle_t_i" name="total_detalle_t_i" value="0">
                                            </tr>

                                        </tbody>
                                    </table>
                                </div>
            </div>
        </div>
    </div>
    

    <div class="col-md-12">
        <div class="input-group">
			<span class="input-group-addon">
		        <i class="fa fa-plus"></i>
			</span>
            <input type="text" id="search_prod" class="form-control" placeholder="Nombre de producto">
        </div>
        <div id="search_results" class="col-md-12">

        </div>
    </div>

    <div class="col-md-12">
        <!-- BEGIN SAMPLE TABLE PORTLET-->

        <div class="portlet box ">
            <div class="portlet-body" style="display: block;">
                <div class="table-scrollable ">
                    <table class="table table-striped table-bordered table-advance table-hover ">
                        <thead>
                        <tr>
                            <th >
                                <i class="fa fa-align-justify"></i> Cantidad </th>
                            <th>
                                <i class="fa fa-briefcase"></i> Detalle </th>
                            
                            <th>
                                <i class="fa fa-dollar"></i> Precio Unitario </th>
                            <th>
                                <i class="fa fa-dollar"></i> Importe </th>

                            <th> Acciones </th>
                        </tr>
                        </thead>
                        <tbody id="caja_detalle">

                        <tfoot>
                                               
                        </tfoot>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- END SAMPLE TABLE PORTLET-->
    </div>

    <div class="col-md-12">
        <div class="form-actions" style="float:right;">
            <button type="button" class="btn default">Cancel</button>
            <button type="submit" class="btn green">Imprimir y Guardar</button>
            <button id="opener">Open Dialog</button>
        </div>
    </div>
    </form>



    <script type="text/javascript">
        var App = {base_url:"http://localhost:8000", site_url:"http://localhost:8000/"};
        var categoria = 1;
        function calcular_totales(){
            total_detalle = 0;
            $("#caja_detalle tr.detalle_tr").each(function(){
                cant =$(this).children('.detalle_td_cantidad').children('.detalle_cantidad').val();
                precio =$(this).children('.detalle_precio').text();
                $(this).children('.detalle_importe').text(cant*precio);
                total_detalle+=cant*precio;
            });
            $('#total_detalle_c').text((total_detalle - (total_detalle*0.18)).toFixed(2));
            $('#total_detalle_c_i').val((total_detalle - (total_detalle*0.18)).toFixed(2));

            $('#total_detalle_c_igv').text((total_detalle*0.18).toFixed(2));
            $('#total_detalle_c_igv_i').val((total_detalle*0.18).toFixed(2));

            $('#total_detalle_t').text(total_detalle.toFixed(2));
            $('#total_detalle_t_i').val(total_detalle.toFixed(2));
        }

        $('#search_prod').keyup(function(event) {
            if($('#search_prod').val().length >3){
                query = $('#search_prod').val();
                $.get( App.site_url+"get_productos/"+categoria+"/"+query+"", function( data ) {
                    $('#search_results').html('');
                    var arr_from_json = JSON.parse( data );
                    $.each(arr_from_json, function(index, value) {
                        $('#search_results').append("<div class='item_to_add' name='"+value.idproducto+"' >"+value.nombre+"</div>")
                    });
                });
            }else{
                $('#search_results').html('');
            }
        });

        $( "#search_results" ).on( "click",".item_to_add", function() {
            $.get( App.site_url+"get_producto_id/"+$(this).attr('name')+"", function( data ) {
                var producto = JSON.parse( data );
                $('#caja_detalle').append('<tr class="detalle_tr"><input type="hidden" name="ids[]" value="'+producto.idproducto+'"><input type="hidden" name="nombres[]" value="'+producto.nombre+'"><input type="hidden" name="precios[]" value="'+producto.precio+'"><td class="detalle_td_cantidad"><input class="detalle_cantidad" name="cantidades[]" type="number"  value="1"></td><td>'+producto.nombre+'</td><td class="detalle_precio">'+producto.precio+'</td><td class="detalle_importe"></td><td><div class="btn btn-outline btn-circle btn-sm red delete_detalle"><i class="fa fa-trash"></i> delete </div></td></tr>');
                calcular_totales();
            });
            $('#search_results').html('');
            $('#search_prod').val('');

        });

        $( "#caja_detalle" ).on( "click",".delete_detalle", function() {
            $(this).closest('tr').remove()
            calcular_totales();
        });

        $( "#caja_detalle" ).on( "keyup mouseup",".detalle_cantidad", function() {
            calcular_totales();
        });

        $('#dni').keyup(function(event){
            if($('#dni').val().length > 7){
                query = $('#dni').val();
                $.get( App.site_url+"get_info_cliente/"+query+"", function( data ) {
                    if(data != 0){
                    var arr_from_json = JSON.parse( data );
                        $('#ruc').val(arr_from_json[0].ruc);
                        $('#nombres_apell').val(arr_from_json[0].nombre +' '+arr_from_json[0].apellido);
                        $('#direccion').val(arr_from_json[0].direccion);
                        $('#idcliente').val(arr_from_json[0].idcliente);
                    }else{
                        $('#idcliente').val('0');
                    }
                });
            }else{
                $('#ruc').val('');
                $('#nombres_apell').val('');
                $('#direccion').val('');
            }
        });
       
    </script>
    @stop