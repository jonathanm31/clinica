@extends('index')
@section('contenido')

<script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
 <meta name="csrf-token" content="{{ csrf_token() }}">
<script src="http://code.jquery.com/ui/1.11.1/jquery-ui.min.js"></script>

<link rel="stylesheet" href="https://code.jquery.com/ui/1.11.1/themes/smoothness/jquery-ui.css" />
<link rel="stylesheet" type="text/css" href="{{ URL::asset('/themes/assets/global/css/lista.css') }}" />
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <a href="../">Home</a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <span>Registro Facturas</span>
            <i class="fa fa-angle-right"></i>
        </li>
       <li>
            <span>
             <?php
                switch ($tipo['tipo']) {
                    case 1:
                        echo "Productos";
                        break;
                    case 2:
                        echo "Pago de Servicios";
                        break;
                    case 3:
                        echo "Rayos X";
                        break;
                } 

             ?>

            </span>
            <i class="fa fa-angle-right"></i>
        </li>
    </ul>
</div>
<div class="col-md-12">
    <div class="portlet light bordered form-fit">
        <h3 class="page-title"> Emisión de Factura
            <small>
                R.U.C. 20455371004</small>
        </h3>
        
        
    </div>
</div>
<form id="form_caja" name="form_caja" >
    <div class="col-md-8">
        <div class="portlet light bordered">
            <div class="portlet-body form">
                <div class="form-body">
                    <h3 class="page-title"> Datos del Cliente </h3>
                    <div class="form-group">
                        <div class="input right margin-top-10 ">
                            <input type="text" max="99999999" class="form-control" id="dni" name="dni" placeholder="DNI">
                            <input type="hidden" id="idcliente" name="idcliente" value="0">
                        </div>

                        <div class="input right margin-top-10 ">
                            <input type="text" max="99999999" class="form-control" id="ruc" name="ruc" placeholder="RUC">
                        </div>

                        <div class="input-icon right margin-top-10">
                            <i class="fa fa-user"></i>
                            <input type="text" class="form-control" id="nombres_apell" name="nombres_apell" placeholder="Nombres y Apellidos">
                        </div>

                        <div class="input-icon right margin-top-10">
                            <i class="fa fa-home"></i>
                            <input type="text" class="form-control" id="direccion" name="direccion" placeholder="Direccion">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-4">
        <div class="portlet light bordered">
            <div class="portlet box green">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-cogs"></i>DATOS FACTURA</div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
                        <a href="javascript:;" class="reload" data-original-title="" title=""> </a>
                    </div>
                </div>
                <div class="portlet-body flip-scroll">
                    <table class="table table-bordered table-striped table-condensed flip-content">

                        <tbody>
                        <tr>
                            <td>VALOR DE VENTA </td>
                            <td style="width: 50%;" id="total_detalle_c"></td>
                            <input type="hidden" id="total_detalle_c_i" name="total_detalle_c_i" value="0">

                        </tr>
                        <tr>
                            <td>IGV DEL 18%</td>
                            <td style="width: 50%;" id="total_detalle_c_igv"></td>
                            <input type="hidden" id="total_detalle_c_igv_i" name="total_detalle_c_igv_i" value="0">
                        </tr>
                        <tr>
                            <td>TOTAL </td>
                            <td style="width: 50%;" id="total_detalle_t"></td>
                            <input type="hidden" id="total_detalle_t_i" name="total_detalle_t_i" value="0">
                        </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>


    <div class="col-md-12">
        <div class="input-group">
			<span class="input-group-addon">
		        <i class="fa fa-plus"></i>
			</span>
            <input type="text" id="search_prod" class="form-control" placeholder="Nombre de producto">
        </div>
        <div id="search_results" class="list-group col-md-12">

        </div>
    </div>

    <div class="col-md-12">
        <!-- BEGIN SAMPLE TABLE PORTLET-->

        <div class="portlet box ">
            <div class="portlet-body" style="display: block;">
                <div class="table-scrollable ">
                    <table class="table table-striped table-bordered table-advance table-hover ">
                        <thead>
                        <tr>
                            <th >
                                <i class="fa fa-align-justify"></i> Cantidad </th>
                            <th>
                                <i class="fa fa-briefcase"></i> Detalle </th>

                            <th>
                                <i class="fa fa-dollar"></i> Precio Unitario </th>
                            <th>
                                <i class="fa fa-dollar"></i> Importe </th>

                            <th> Acciones </th>
                        </tr>
                        </thead>
                        <tbody id="caja_detalle">

                        <tfoot>

                        </tfoot>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- END SAMPLE TABLE PORTLET-->
    </div>



    <div class="col-md-12">
        <div class="form-actions" style="float:right;">
            <button type="button" id="btn-verificar" class="btn green">Verificar</button>
        </div>
    </div>
</form>

<div id="verficarpop" > 
</div>


<script type="text/javascript">
    var App = {base_url:"http://localhost:8000/", site_url:"http://localhost:8000/"};
    var categoria = <?php echo $tipo['tipo'] ?>;//aqui imprimir la variable php de categoria
    function calcular_totales(){
        total_detalle = 0;
        $("#caja_detalle tr.detalle_tr").each(function(){
            cant =$(this).children('.detalle_td_cantidad').children('.detalle_cantidad').val();
            precio =$(this).children('.detalle_td_precio').children('.detalle_precio').val();
            $(this).children('.detalle_importe').text((cant*precio).toFixed(2));
            total_detalle+=cant*precio;
        });
        $('#total_detalle_c').text((total_detalle - (total_detalle*0.18)).toFixed(2));
        $('#total_detalle_c_i').val((total_detalle - (total_detalle*0.18)).toFixed(2));

        $('#total_detalle_c_igv').text((total_detalle*0.18).toFixed(2));
        $('#total_detalle_c_igv_i').val((total_detalle*0.18).toFixed(2));

        $('#total_detalle_t').text(total_detalle.toFixed(2));
        $('#total_detalle_t_i').val(total_detalle.toFixed(2));
    }

    $('#search_prod').keyup(function(event) {

        if($('#search_prod').val().length >3){
            query = $('#search_prod').val();
            $.get(App.site_url+"get_productos/"+categoria+"/"+query+"", function( data ) {
                $('#search_results').html('');
                var arr_from_json = JSON.parse( data );
                $.each(arr_from_json, function(index, value) {
                    $('#search_results').append("<div id='item_to_add' class='list-group-item' name='"+value.idproducto+"' >"+value.nombre+"</div>")
                });
            });
        }else{
            $('#search_results').html('');
        }
    });

    $('#item_to_add').hover(function() {
            $( this ).toggleClass( "active" );
          }, function() {
            $( this ).removeClass( "active" );
          }
        );

    $( "#search_results" ).on( "click","#item_to_add", function() {

        $.get( App.site_url+"get_producto_id/"+$(this).attr('name')+"", function( data ) {
            var producto = JSON.parse( data );
            $('#caja_detalle').append('<tr class="detalle_tr"><input type="hidden" name="ids[]" id ="idProductos"value="'+producto.idproducto+'"><input type="hidden" name="nombres[]" id= "nombreProd" value="'+producto.nombre+'"><td class="detalle_td_cantidad"><input class="detalle_cantidad" name="cantidades[]" type="number"  value="1"></td><td>'+producto.nombre+'</td><td class="detalle_td_precio"><input class="detalle_precio" name="precios[]" type="number"  value="'+producto.precio+'"></td><td class="detalle_importe"></td><td><div class="btn btn-outline btn-circle btn-sm red delete_detalle"><i class="fa fa-trash"></i> delete </div></td></tr>');
            calcular_totales();
        });
        $('#search_results').html('');
        $('#search_prod').val('');

    });

    $( "#caja_detalle" ).on( "click",".delete_detalle", function() {
        $(this).closest('tr').remove()
        calcular_totales();
    });

    $( "#caja_detalle" ).on( "keyup mouseup",".detalle_cantidad", function() {
        calcular_totales();
    });

    $( "#caja_detalle" ).on( "keyup mouseup",".detalle_precio", function() {
        calcular_totales();
    });

    $('#dni').keyup(function(event){
        if($('#dni').val().length > 7){
            query = $('#dni').val();
            $.get( App.site_url+"get_info_cliente/"+query+"", function( data ) {
                if(data != 0){
                    var arr_from_json = JSON.parse( data );
                    $('#ruc').val(arr_from_json[0].ruc);
                    $('#nombres_apell').val(arr_from_json[0].nombre);
                    $('#direccion').val(arr_from_json[0].direccion);
                    $('#idcliente').val(arr_from_json[0].idcliente);
                }else{
                    $('#idcliente').val('0');
                }
            });
        }else{
            $('#ruc').val('');
            $('#nombres_apell').val('');
            $('#direccion').val('');
        }
    });

    $("#verficarpop").dialog({
        autoOpen: false
    });

    $( "#btn-verificar" ).click(function() {
        /*DATOS CLIENTE*/
        var idcliente = $('#idcliente').val();
        var dni = $('#dni').val();
        var ruc = $('#ruc').val();
        var nombres_apell = $('#nombres_apell').val();
        var direccion = $('#direccion').val();

        /*DATOS CABECERA FACTURA*/
        var total_detalle_c_i = $('#total_detalle_c_i').val();
        var total_detalle_c_igv_i = $('#total_detalle_c_igv_i').val();
        var total_detalle_t_i = $('#total_detalle_t_i').val();


        /*DATOS DETALLE FACTURA*/

        var cantidad = new Array();
        var precio = new Array();
        var importe = new Array();
        var producto = new Array();
        var idproducto = new Array();


        $("#caja_detalle tr.detalle_tr").each(function(){
            var cat = $(this).children('.detalle_td_cantidad').children('.detalle_cantidad').val();
            var pre = $(this).children('.detalle_td_precio').children('.detalle_precio').val();
            var nomP = $(this).children('#nombreProd').val();
            var idP = $(this).children('#idProductos').val();
            cantidad.push(cat);
            precio.push(pre);
            importe.push((cat*pre).toFixed(2));
            producto.push(nomP);
            idproducto.push(idP);

        });

        $("#verficarpop").empty();
        $("#verficarpop").append(
            '<h2>Datos del Cliente</h2><div class="table-responsive"><table class="table"><tbody><tr><td> <h4>DNI<h4> </td><td> '+dni+'</td></tr><tr><td> <h4>RUC</h4> </td><td> '+ruc+'</td></tr><tr><td><h4> Nombres y Apellidos </h4></td><td> '+nombres_apell+'</td></tr><tr><td><h4>Direccion </h4></td><td> '+direccion+'</td></tr></tbody></table></div>'
            );
        var detalle = '<h2>Detalle de Factura</h2><div class="table-responsive"><table class="table"><tbody><thead><tr><td><h4>Cantidad</h4></td><td><h4>Detalle</h4></td><td><h4>Precio</h4></td><td><h4>Importe</h4></td></thead>';
        for (i = 0; i < cantidad.length; i++) { 
                     detalle += '<tr><td>'+cantidad[i]+'</td><td>'+producto[i]+'</td><td>'+precio[i]+'</td><td>'+importe[i]+'</td></tr>';
                    }
        $("#verficarpop").append(detalle+'</tbody></table></div>');

        $("#verficarpop").append(
            '<h2>Monto Total</h2><div class="table-responsive"><table class="table"><tbody><tr><td><h4> Valor de venta </h4></td><td> '+total_detalle_c_i+'</td></tr><tr><td> <h4>IGV </h4></td><td> '+total_detalle_c_igv_i+'</td></tr><tr><td><h4> Total </h4> </td><td> '+total_detalle_t_i+'</td></tr></tbody></table></div>'
            );

        var opt = {
        autoOpen: true,
        modal: true,
        width: 550,
        height:650,
        title: 'Datos Factura',
        buttons: {
            "Guardar": function(event) {
              $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type: 'POST',
                    url: App.site_url+"guardar_caja_fact/",
                    data: {idcliente:idcliente, dni:dni,ruc:ruc, nombres_apell:nombres_apell, direccion:direccion, cantidades:cantidad, total_detalle_t_i:total_detalle_t_i, ids:idproducto, precios:precio , tipo:categoria },
                    dataType: 'json',
                    beforeSend: function() {
                        
                        
                    },
                    success:function(data){

                    $("#verficarpop").empty();    
                    $("#verficarpop").append('<div class="custom-alerts alert alert-info fade in"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>Se guardo Correctamente</div>');
                    
                    window.open(App.site_url+"get_info_factura/"+data, '_blank',"width=800, height=600");
                    location.reload();


                          
                    },
                    error:function(){
                        $("#verficarpop").append('<div class="custom-alerts alert alert-danger fade in"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>Error con el Servidor, intente de nuevo mas tarde.</div>');
                    },

                });
            },
            Cancel: function() {
               
              $( this ).dialog( "close" );
                }
            }
        };

        var theDialog = $("#verficarpop").dialog(opt);
        theDialog.dialog("open");



    });

</script>

@stop