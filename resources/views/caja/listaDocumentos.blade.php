@extends('index')
@section('contenido')
<link rel="stylesheet" type="text/css" href="{{ URL::asset('/themes/assets/global/css/print.css') }}" media="print" />

<div class="portlet box green">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-cogs"></i>Reporte de Pagos al Dia <?php echo  date("Y-m-d")  ?></div>
                                    <div class="tools">
                                        <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
                                        <a href="javascript:;" class="reload" data-original-title="" title=""> </a>
                                    </div>
                                </div>
                                <div class="portlet-body flip-scroll">
                                    <table class="table table-bordered table-striped table-condensed flip-content">
                                        <thead class="flip-content">
                                            <tr>
                                                <th width="20%"> Tipo </th>
                                                <th> Serie </th>
                                                <th class="numeric"> Numeracion </th>
                                                <th class="numeric"> Cliente </th>
                                                <th class="numeric"> DNI Cliente </th>
                                                <th class="numeric"> Tipo Pago </th>
                                                <th class="numeric"> Monto Total</th>
                                                <th class="numeric"> Fecha de Creacion </th>
                                                
                                            </tr>
                                        </thead>
                                        <tbody>
                                         @foreach($documentos as $documento)
                                            <tr>
                                                <td> <?php
								                switch ($documento->tipo) {
								                    case 1:
								                        echo "Factura";
								                        break;
								                    case 2:
								                        echo "Boleta";
								                        break;
								                    case 3:
								                        echo "Nota de credito";
								                        break;
								                    case 4:
								                    	echo "Boleta Fisica";
								                    	break;
								                } 

								             ?>
											 </td>
                                                <td> <?php echo str_pad($documento->serie, 3, "0", STR_PAD_LEFT);    ?> </td>
                                                <td class="numeric"> {{$documento->numeracion}} </td>
                                                <td class="numeric"> {{$documento->nombre}} </td>
                                                <td class="numeric"> {{$documento->dni}} </td>

                                                 <td class="numeric"> 
    <?php
                                                switch ($documento->pago) {
                                                    case 1:
                                                        echo "Visa";
                                                        break;
                                                    case 2:
                                                        echo "Mastercard";
                                                        break;
                                                    case 3:
                                                        echo "American Express";
                                                        break;
                                                    case 4:
                                                        echo "Transferencia Bancaria";
                                                    case 5:
                                                        echo "Efectivo";
                                                        break;
                                                } 
?>
                                                 </td>


                                                <td class="numeric"> S/. {{$documento->total}} </td>
                                                <td class="numeric"> <?php 
                                                setlocale(LC_TIME, "es_PE");
                                                date_default_timezone_set('America/Lima');
                                                $time = strtotime($documento->created_at);
                                                $fecha = date("F j, Y, g:i a",$time);
                                                echo $fecha;
                                                ?> </td>
                                            
                                            </tr>
                                         @endforeach
                                            
                                        </tbody>
                                    </table>
                                  {!!$documentos->render()!!}

                                </div>
                            </div>
<div class="col-md-12 " >
    <button class ="btn green no-print"onclick="myFunction()"><i class="fa fa-print">  Imprimir  </i></button> 
</div>
<script>
function myFunction() {
    window.print();
}
</script>


@stop